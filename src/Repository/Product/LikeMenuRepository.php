<?php

namespace App\Repository\Product;

use App\Entity\Product\LikeMenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LikeMenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method LikeMenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method LikeMenu[]    findAll()
 * @method LikeMenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeMenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LikeMenu::class);
    }

    // /**
    //  * @return likeMenu[] Returns an array of likeMenu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?likeMenu
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
