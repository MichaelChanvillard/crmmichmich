<?php

namespace App\Repository\Product;

use App\Entity\Product\TypeProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeProduct[]    findAll()
 * @method TypeProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeProduct::class);
    }

    // /**
    //  * @return typeProduct[] Returns an array of typeProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?typeProduct
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
