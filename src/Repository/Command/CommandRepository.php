<?php

namespace App\Repository\Command;

use App\Entity\Command\Command;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Command|null find($id, $lockMode = null, $lockVersion = null)
 * @method Command|null findOneBy(array $criteria, array $orderBy = null)
 * @method Command[]    findAll()
 * @method Command[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Command::class);
    }

    public function getLastNumberOfFinancialYear(string $financialYear): int
    {
        $query = $this->createQueryBuilder('c')
            ->select('MAX(c.number) as lastNumber')
            ->where('c.financialYear = :financialYear')
            ->setParameter('financialYear', $financialYear);

        return (int) $query->getQuery()->getOneOrNullResult()['lastNumber'];
    }

    public function getCommandNotDelivred()
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.isDelivred = :delivred')
            ->andWhere('c.isActive = :active')
            ->setParameter('delivred', false)
            ->setParameter('active', true)
            ->getQuery()
            ->getResult();

        return $query;
    }

    public function getCommandIsDeliveredNotPaid()
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.isDelivred = :delivred')
            ->andWhere('c.isPaid = :paid')
            ->andWhere('c.isActive = :active')
            ->setParameter('delivred', false)
            ->setParameter('active', true)
            ->setParameter('paid', false )
            ->getQuery()
            ->getResult();

        return $query;

    }

    // /**
    //  * @return command[] Returns an array of command objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?command
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
