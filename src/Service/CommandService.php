<?php

namespace App\Service;

use App\Entity\Command\Command;
use App\Entity\Command\Invoice;
use App\Repository\Command\InvoiceRepository;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;

class CommandService extends AbstractController
{
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(Filesystem $filesystem, InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->filesystem = $filesystem;
    }

    /**
     * @param Command $commande
     */
    public function generateInvoice($commande)
    {
        $currentDate = new \DateTime();
        $financialYear = $currentDate->format('Y-m-d');
        // Définition du numéro du devis
        $number = $this->invoiceRepository->getLastNumberOfFinancialYear($financialYear) + 1;

        $invoice = new Invoice();
        $invoice->setCommand($commande);
        $invoice->setFinancialYear($financialYear);
        $invoice->setNumber($number);
        $invoice->setPaidAt($currentDate);
        $invoice->setIsPaid(true);
        $invoice->setInvoicePdf($financialYear . '-' . $number . '.pdf');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($invoice);
        $entityManager->flush();
        $this->generateInvoicePfd($invoice);

    }
    /**
     * @param Invoice $invoice
     */
    public function  generateInvoicePfd($invoice)
    {
        $invoiceFinancialYearDirectory =   'media/invoice/' . $invoice->getFinancialYear();
        $invoicePdfFilename = $invoiceFinancialYearDirectory . '/' . $invoice->getFinancialYear() . '-' . $invoice->getNumber() . '.pdf';

        if(!$this->filesystem->exists($invoicePdfFilename)){
            $dompdf = new Dompdf();

            $actionRecordHtml = $this->renderView('pdf/pdf_template.html.twig', [
                'invoice' => $invoice,
                'invoiceTotalPriceHt' => $this->generateTotalPriceHt($invoice),
                'invoiceTotalTva' => $this->generateTotalTva($invoice),
                'invoiceTotalPriceTtc' => $this->generatetotalpricettc($invoice->getCommand()),

            ]);

            $dompdf->loadHtml($actionRecordHtml);
            $dompdf->render();

            if (!$this->filesystem->exists($invoiceFinancialYearDirectory)) {
                $this->filesystem->mkdir($invoiceFinancialYearDirectory, 0755);
            }
            // Sauvegarde de la facture sur le serveur
            file_put_contents($invoicePdfFilename, $dompdf->output());

            $invoice->setInvoicePdf($invoicePdfFilename);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }


    }

    /**
     * @param Invoice $invoice
     */
    public function generateTotalPriceHt($invoice)
    {
        $orderItems = $invoice->getCommand()->getOrderItems();
        $totalPriceHt = 0;

        foreach ($orderItems as $orderItem){
            $totalPriceHt = $totalPriceHt + ($orderItem->getPriceHt() * $orderItem->getQuantity());

        }

        return number_format($totalPriceHt, 2);
    }

    /**
     * @param Invoice $invoice
     */
    public function generateTotalTva($invoice){
        $orderItems =  $invoice->getCommand()->getOrderItems();
        $totalTva = 0;

        foreach ($orderItems as $orderItem){
            $totalTva = $totalTva + ($orderItem->getQuantity() * (($orderItem->getPriceHt()/100) * $orderItem->getTaxe()));

        }

        return number_format($totalTva, 2);

    }

    /**
     * @param Command $commande
     */
    public function generatetotalpricettc($commande)
    {
        $products = $commande->getOrderItems();

        $totalPriceTtc = 0;
        foreach ($products as $product){
            $totalPriceTtc = $totalPriceTtc +($product->getQuantity() * ($product->getPriceHt() + ($product->getPriceHt()/100) * ($product->getTaxe())));

        }

        return number_format($totalPriceTtc, 2);
    }
}