<?php

namespace App\Service;

use App\Entity\User\User;
use App\Form\User\PasswordUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class UserService extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
       $this->passwordEncoder = $passwordEncoder;
    }

    public function changePassword(Request $request,User $user, $route)
    {
        $form = $this->createForm(PasswordUserType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre mot passe a été changé !');
            return $this->redirectToRoute($route);
        }

        return $this->render('user/user/password.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}