<?php

namespace App\Entity\Command;


use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\Product;
use App\Entity\User\User;
use App\Repository\Command\CommandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CommandRepository::class)
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $number;

    /**
     * Exercice du devis.
     *
     * @ORM\Column(type="string")
     */
    private $financialYear;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    private $creatAt;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $isEatTakeAway;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $isDelivred;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    Private $isPaid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=Invoice::class, mappedBy="command")
     */
    private $invoices;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commands")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="command", cascade={"persist"})
     */
    private $orderItems;

    public function __construct()
    {
        $this->isEatTakeAway = true;
        $this->isActive = false;
        $this->isPaid = false;
        $this->creatAt = new \DateTime();
        $this->isDelivred = false;
        $this->invoices = new ArrayCollection();
        $this->orderItems = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatAt(): ?\DateTimeInterface
    {
        return $this->creatAt;
    }

    public function setCreatAt(\DateTimeInterface $creatAt): self
    {
        $this->creatAt = $creatAt;

        return $this;
    }

    public function getIsEatTakeAway(): ?bool
    {
        return $this->isEatTakeAway;
    }

    public function setIsEatTakeAway(bool $isEatTakeAway): self
    {
        $this->isEatTakeAway = $isEatTakeAway;

        return $this;
    }

    /**
     * @return Collection|invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCommand($this);
        }

        return $this;
    }

    public function removeInvoice(invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getCommand() === $this) {
                $invoice->setCommand(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinancialYear()
    {
        return $this->financialYear;
    }

    /**
     * @param mixed $financialYear
     */
    public function setFinancialYear($financialYear): void
    {
        $this->financialYear = $financialYear;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDelivred()
    {
        return $this->isDelivred;
    }

    /**
     * @param mixed $isDelivred
     */
    public function setIsDelivred($isDelivred): void
    {
        $this->isDelivred = $isDelivred;
    }

    public function changeDelivered($isDelivered)
    {
        $isDelivered->setIsDelivred(!$isDelivered->getIsDelivred());
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setCommand($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getCommand() === $this) {
                $orderItem->setCommand(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param mixed $isPaid
     */
    public function setIsPaid($isPaid): void
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @param User $user
     * @param CommandRepository $commandRepository
     */
    static public function createCommand($user, CommandRepository $commandRepository)
    {
        $command = new self();

        $dateTime = new \DateTime();
        $financialYear = $dateTime->format('Y-m-d');

        $number = $commandRepository->getLastNumberOfFinancialYear($financialYear) + 1;

        $command->setCreatAt($dateTime);
        $command->setFinancialYear($financialYear);
        $command->setNumber($number);
        if(!is_null($user)){
        $command->setUser($user);
        }

        return $command;
    }

    /**
     * @param OrderItem $productOrderItem
     * @param Command $command
     * @param Product $product
     * @param $quantity
     */
    public function addOrderProductTocommand($productOrderItem, $command, $product, $quantity)
    {

        $productOrderItem->setTaxe($product->getProductType()->getTaxeEatTakeOut());
        $productOrderItem->setPriceHt($product->getPriceHt());
        $productOrderItem->setNameProduct($product->getName());
        $productOrderItem->setProduct($product) ;
        $productOrderItem->setCommand($command);
        $productOrderItem->setQuantity($quantity);
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @param Command $command
     */
    public function generateTotalePriceCommandeHt($command)
    {
        $totalPriceHt = 0;
        foreach ($command->getOrderItems() as $oderItem){

            $totalPriceHt += ($oderItem->getQuantity() * $oderItem->getPriceHt());

        }

        return$totalPriceHt;

    }

    /**
     * @param Command $command
     */
    public function generateTotalePriceCommandeTaxe($command)
    {

        $totaltaxe = 0;

        foreach ($command->getOrderItems() as $oderItem){

            $totaltaxe += ($oderItem->getQuantity() * $oderItem->getValueTaxeOutSite());
        }

        return $totaltaxe;

    }

    /**
     * @param Command $command
     */
    public function generateTotalePriceCommandeTtc($command)
    {
        $totalPriceTtc = 0;


        foreach ($command->getOrderItems() as $oderItem){

            $totalPriceTtc += ($oderItem->getQuantity() * $oderItem->getPriceTtcOutSite());
        }

        return $totalPriceTtc;

    }

}
