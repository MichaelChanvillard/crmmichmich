<?php

namespace App\Entity\Command;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\Menu;
use App\Entity\Product\Product;
use App\Repository\Command\OrderItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="orderItems")
     */
    private $menu;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orderItems")
     */
    private $product;

    /**
     * @ORM\Column(type="float")
     */
    private $priceHt;

    /**
     * @ORM\Column(type="float")
     */
    private $taxe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameProduct;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountMenu;

    /**
     * @ORM\ManyToOne(targetEntity=Command::class, inversedBy="orderItems")
     */
    private $command;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPriceHt(): ?float
    {
        return $this->priceHt;
    }

    public function setPriceHt(float $priceHt): self
    {
        $this->priceHt = $priceHt;

        return $this;
    }

    public function getTaxe(): ?float
    {
        return $this->taxe;
    }

    public function setTaxe(float $taxe): self
    {
        $this->taxe = $taxe;

        return $this;
    }

    public function getProductOderitems(): ?Command
    {
        return $this->productOderitems;
    }

    public function setProductOderitems(?Command $productOderitems): self
    {
        $this->productOderitems = $productOderitems;

        return $this;
    }

    public function getNameProduct(): ?string
    {
        return $this->nameProduct;
    }

    public function setNameProduct(string $nameProduct): self
    {
        $this->nameProduct = $nameProduct;

        return $this;
    }

    public function getDiscountMenu(): ?float
    {
        return $this->discountMenu;
    }

    public function setDiscountMenu(?float $discountMenu): self
    {
        $this->discountMenu = $discountMenu;

        return $this;
    }

    public function getCommand(): ?Command
    {
        return $this->command;
    }

    public function setCommand(?Command $command): self
    {
        $this->command = $command;

        return $this;
    }
    public function getPriceTtcOutSite (){
        $totalPriceTtc = ($this->priceHt +(($this->getPriceHt()/100) * $this->getTaxe()) );
        return number_format($totalPriceTtc, 2);
    }

    public function getValueTaxeOutSite (){
        $totalPriceTtc = (($this->getPriceHt()/100) * $this->getTaxe());
        return number_format($totalPriceTtc, 2);
    }
}
