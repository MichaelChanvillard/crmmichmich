<?php

namespace App\Entity\Command;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Command\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=255)
     * @Assert\NotBlank
     */
    private $number;

    /**
     * Exercice du la facture.
     *
     * @ORM\Column(type="string")
     */
    private $financialYear;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $isPaid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank
     */
    private $paidAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank
     */
    private $sendEmailAt;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $isInvoiceCanceled;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank
     */
    private $isCanceledAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $invoicePdf;

    /**
     * @ORM\ManyToOne(targetEntity=Command::class, inversedBy="invoices")
     */
    private $command;

    public function __construct()
    {
        $this->isInvoiceCanceled = false;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): self
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getPaidAt(): ?\DateTimeInterface
    {
        return $this->paidAt;
    }

    public function setPaidAt(\DateTimeInterface $paidAt): self
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    public function getSendEmailAt(): ?\DateTimeInterface
    {
        return $this->sendEmailAt;
    }

    public function setSendEmailAt(\DateTimeInterface $sendEmailAt): self
    {
        $this->sendEmailAt = $sendEmailAt;

        return $this;
    }

    public function getIsInvoiceCanceled(): ?bool
    {
        return $this->isInvoiceCanceled;
    }

    public function setIsInvoiceCanceled(bool $isInvoiceCanceled): self
    {
        $this->isInvoiceCanceled = $isInvoiceCanceled;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getIsCanceledAt()
    {
        return $this->isCanceledAt;
    }

    /**
     * @param mixed $isCanceledAt
     */
    public function setIsCanceledAt($isCanceledAt): void
    {
        $this->isCanceledAt = $isCanceledAt;
    }


    public function getInvoicePdf(): ?string
    {
        return $this->invoicePdf;
    }

    public function setInvoicePdf(string $invoicePdf): self
    {
        $this->invoicePdf = $invoicePdf;

        return $this;
    }

    public function getCommand(): ?command
    {
        return $this->command;
    }

    public function setCommand(?command $command): self
    {
        $this->command = $command;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinancialYear()
    {
        return $this->financialYear;
    }

    /**
     * @param mixed $financialYear
     */
    public function setFinancialYear($financialYear): void
    {
        $this->financialYear = $financialYear;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

}
