<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Product\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Please enter a name tag"
     * )
     * @Assert\Length(
     *     min= 3,
     *     minMessage="The name tag should be at least {{ limit }} characters",
     *     max= 20,
     *     maxMessage="The name tag must contain at most {{ limit }} characters",
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    private $createAt;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="tags")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity=Menu::class, inversedBy="tags")
     */
    private $menus;

    /**
     * Tag constructor.
     */
    public function __construct()
    {
        $this->setCreateAt(new \DateTime());
        $this->products = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    /**
     * @return Collection|menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
        }

        return $this;
    }

    public function removeMenu(menu $menu): self
    {
        if ($this->menus->contains($menu)) {
            $this->menus->removeElement($menu);
        }

        return $this;
    }
}
