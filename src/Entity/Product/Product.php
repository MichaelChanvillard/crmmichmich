<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Command\OrderItem;
use App\Entity\User\User;
use App\Repository\Product\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Please enter a name product"
     * )
     * @Assert\Length(
     *     min= 5,
     *     minMessage="The name produit should be at least {{ limit }} characters",
     *     max= 50,
     *     maxMessage="The name produit tag must contain at most {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *     min= 5,
     *     minMessage="The description should be at least {{ limit }} characters",
     *     max= 255,
     *     maxMessage="The description must contain at most {{ limit }} characters",
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *     message="Please enter a price ht"
     * )
     */
    private $priceHt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=LikeProduct::class, mappedBy="product")
     * @Assert\NotBlank
     */
    private $likeProducts;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, mappedBy="products")
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity=Menu::class, mappedBy="products")
     */
    private $menus;

    /**
     * @ORM\ManyToOne(targetEntity=TypeProduct::class, inversedBy="products")
     */
    private $productType;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="product")
     */
    private $orderItems;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->likeProducts = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPriceHt(): ?float
    {
        return $this->priceHt;
    }

    public function setPriceHt(float $priceHt): self
    {
        $this->priceHt = $priceHt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|likeProduct[]
     */
    public function getLikeProducts(): Collection
    {
        return $this->likeProducts;
    }

    public function addLikeProduct(likeProduct $likeProduct): self
    {
        if (!$this->likeProducts->contains($likeProduct)) {
            $this->likeProducts[] = $likeProduct;
            $likeProduct->setProduct($this);
        }

        return $this;
    }

    public function removeLikeProduct(likeProduct $likeProduct): self
    {
        if ($this->likeProducts->contains($likeProduct)) {
            $this->likeProducts->removeElement($likeProduct);
            // set the owning side to null (unless already changed)
            if ($likeProduct->getProduct() === $this) {
                $likeProduct->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addProduct($this);
        }

        return $this;
    }

    public function removeTag(tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->addProduct($this);
        }

        return $this;
    }

    public function removeMenu(menu $menu): self
    {
        if ($this->menus->contains($menu)) {
            $this->menus->removeElement($menu);
            $menu->removeProduct($this);
        }

        return $this;
    }

    public function getProductType(): ?TypeProduct
    {
        return $this->productType;
    }

    public function setProductType(?TypeProduct $productType): self
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setProduct($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getProduct() === $this) {
                $orderItem->setProduct(null);
            }
        }

        return $this;
    }

    public function getPriceTtcOneSite (){
        $totalPriceTtc = ($this->priceHt +(($this->getPriceHt()/100) * $this->getProductType()->getTaxeEatOnSite()) );
        return number_format($totalPriceTtc, 2);
    }

    public function getPriceTtcOutSite (){
        $totalPriceTtc = ($this->priceHt +(($this->getPriceHt()/100) * $this->getProductType()->getTaxeEatTakeOut()) );
        return number_format($totalPriceTtc, 2);
    }

    /**
     * User as liker Product ?
     * @param User $user
     * @return bool
     */
    public function isLikedByUser(User $user): bool {
        foreach ($this->getLikeProducts() as $likes){
            if($likes->getUser() === $user){
                return true;
            }
        }
        return false;
    }
}
