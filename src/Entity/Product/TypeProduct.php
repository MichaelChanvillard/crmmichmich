<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Product\TypeProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypeProductRepository::class)
 */
class TypeProduct
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *     message="Please enter a tax eat on site"
     * )
     */
    private $taxeEatOnSite;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *     message="Please enter a taxe eat take out"
     * )
     */
    private $taxeEatTakeOut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="productType")
     */
    private $products;

    /**
     * Liste des types possibles du devis.
     */
    public const PRODCUT_TYPES =
        [
            1 =>'plats' ,
            2 => 'desserts',
            3 => 'sodas',
            4 => 'alcools',
            5 => 'cafes',
            6 => 'eaux',
        ];

    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return self::PRODCUT_TYPES[$this->getProductType()];
    }
    public function getProductType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTaxeEatOnSite(): ?float
    {
        return $this->taxeEatOnSite;
    }

    public function setTaxeEatOnSite(float $taxeEatOnSite): self
    {
        $this->taxeEatOnSite = $taxeEatOnSite;

        return $this;
    }

    public function getTaxeEatTakeOut(): ?float
    {
        return $this->taxeEatTakeOut;
    }

    public function setTaxeEatTakeOut(float $taxeEatTakeOut): self
    {
        $this->taxeEatTakeOut = $taxeEatTakeOut;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setProductType($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getProductType() === $this) {
                $product->setProductType(null);
            }
        }

        return $this;
    }

}
