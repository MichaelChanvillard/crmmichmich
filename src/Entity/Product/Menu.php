<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Command\OrderItem;
use App\Repository\Product\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Please enter a name menu"
     * )
     * @Assert\Length(
     *     min= 3,
     *     minMessage="The name menu should be at least {{ limit }} characters",
     *     max= 255,
     *     maxMessage="The name menu must contain at most {{ limit }} characters",
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(
     *     message="Please enter a description menu"
     * )
     * @Assert\Length(
     *     min= 5,
     *     minMessage="The description should be at least {{ limit }} characters",
     *     max= 255,
     *     maxMessage="The description must contain at most {{ limit }} characters",
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *     message="Please enter a number for discount menu"
     * )
     */
    private $discountMenu;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=LikeMenu::class, mappedBy="menu")
     */
    private $likeMenus;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, mappedBy="menus")
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="menus")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="menu")
     */
    private $orderItems;

    public function __toString()
    {
   return $this->name;
    }


    public function __construct()
    {
        $this->createAt = new \DateTime();
        $this->likeMenus = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|likeMenu[]
     */
    public function getLikeMenus(): Collection
    {
        return $this->likeMenus;
    }

    public function addLikeMenu(likeMenu $likeMenu): self
    {
        if (!$this->likeMenus->contains($likeMenu)) {
            $this->likeMenus[] = $likeMenu;
            $likeMenu->setMenu($this);
        }

        return $this;
    }

    public function removeLikeMenu(likeMenu $likeMenu): self
    {
        if ($this->likeMenus->contains($likeMenu)) {
            $this->likeMenus->removeElement($likeMenu);
            // set the owning side to null (unless already changed)
            if ($likeMenu->getMenu() === $this) {
                $likeMenu->setMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addMenu($this);
        }

        return $this;
    }

    public function removeTag(tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeMenu($this);
        }

        return $this;
    }

    /**
     * @return Collection|product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscountMenu()
    {
        return $this->discountMenu;
    }

    /**
     * @param mixed $discountMenu
     */
    public function setDiscountMenu($discountMenu): void
    {
        $this->discountMenu = $discountMenu;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setMenu($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getMenu() === $this) {
                $orderItem->setMenu(null);
            }
        }

        return $this;
    }
}
