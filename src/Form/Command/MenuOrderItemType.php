<?php

namespace App\Form\Command;

use App\Entity\Command\OrderItem;
use App\Entity\Product\Menu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuOrderItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('quantity')
            ->add('menu', EntityType::class,[
                'class' => Menu::class,
                'placeholder' => '-------------------',
                'label' => false,
                'mapped' => false,
            ])

        ;

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
          function (FormEvent $events) use ($options){

//                dump('parent',$events);
                $form = $events->getForm()->getData();
//                dump('data form',$form);
                $form->setCommand($options['command']);
                foreach ($events->getForm()->all()['menu']->getData()->getProducts()->getValues() as $product){

                    $form->setmenu($events->getForm()->all()['menu']->getData());
                    $form->setProduct($product);
                    $form->setDiscountMenu($events->getForm()->all()['menu']->getData()->getDiscountMenu());
                    $form->setPriceHt($product->getPriceHt());
//                    $form->setTaxe($product->getPr)
                    $form->setNameProduct($product->getName());
                    $form->setTaxe(2.5);
                    //dump('menu product',$form);
                }



//dump($events->getForm()->all()['menu']->getData());
//dump($events->getForm()->all()['menu']->getData()->getProducts()->getValues());
//dump($form);


          }
        );

    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,

        ]);
        $resolver->setRequired('command');

    }
}
