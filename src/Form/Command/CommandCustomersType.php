<?php

namespace App\Form\Command;

use App\Repository\Command\OrderItemRepository;
use App\Repository\Product\TypeProductRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CommandCustomersType extends AbstractType
{
    /**
     * @var OrderItemRepository
     */
    private $typeProductRepository;

    /**
     * @var CommandCustomerEventSubscriberInterface
     */
    private $commandCustomerEventSubscriberInterface;


    public function __construct(CommandCustomerEventSubscriberInterface $commandCustomerEventSubscriberInterface, TypeProductRepository $typeProductRepository)
    {
        $this->commandCustomerEventSubscriberInterface = $commandCustomerEventSubscriberInterface;
        $this->typeProductRepository = $typeProductRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->typeProductRepository->findAll() as $typeProduct){
            $builder
                ->add($typeProduct->getType(), AddProuctListeCommandCustomer::class,[
                    'products' => $typeProduct->getProducts(),
                ])
                ->addEventSubscriber($this->commandCustomerEventSubscriberInterface);

        }

    }

}