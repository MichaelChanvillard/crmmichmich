<?php

namespace App\Form\Command;

use App\Entity\Command\Command;
use App\Entity\Command\OrderItem;
use App\Repository\Command\CommandRepository;
use App\Repository\Product\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CommandCustomerEventSubscriberInterface implements EventSubscriberInterface
{

    /**
     * @var Security
     */
    private $security;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CommandRepository
     */
    private $commandRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RequestStack
     */
    private $requestStack;



    public function __construct( CommandRepository $commandRepository, ProductRepository $productRepository,Security $security, EntityManagerInterface $entityManager, RequestStack $requestStack)
    {

        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->security =$security;
        $this->productRepository = $productRepository;
        $this->commandRepository = $commandRepository;

    }
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::SUBMIT => 'eventSubmit',

        ];
    }

    public function eventSubmit(FormEvent $event)
    {
            $command = Command::createCommand( $this->security->getUser(),$this->commandRepository);

            foreach ( $event->getData() as $typeProducts){
                foreach ($typeProducts as $productId => $quantity)

                    if($quantity != 0){

                        $productOrderItem =new OrderItem();
                        $product = $this->productRepository->find($productId);
                        $command->addOrderProductTocommand($productOrderItem, $command, $product, $quantity);
                        $this->entityManager->persist($productOrderItem);

                    }
            }
            $this->entityManager->persist($command);
            $this->entityManager->flush();


            $this->entityManager->refresh($command);
            $request = $this->requestStack->getCurrentRequest();
            $request->attributes->set('command', $command);

        }

}