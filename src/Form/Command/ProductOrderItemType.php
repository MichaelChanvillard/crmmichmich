<?php

namespace App\Form\Command;

use App\Entity\Command\OrderItem;
use App\Entity\Product\Menu;
use App\Entity\Product\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductOrderItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('quantity')
            ->add('product', EntityType::class,[
                'class' => Product::class,
                'placeholder' => '-------------------',
                'label' => false

            ])

        ;

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $events) use ($options){
                $form = $events->getForm()->getData();
                $form->setCommand($options['command']);

            }
        );

    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,

        ]);
        $resolver->setRequired('command');

    }
}