<?php

namespace App\Form\Command;


use App\Entity\Product\Product;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;


class AddProuctListeCommandCustomer extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Product $product */
        $products = $options['products'];

        foreach ($products as $product){

            if($product->getIsActive() == true){
                $builder
                    ->add($product->getId() , NumberType::class,[

                        'label' =>  false,
                        'input' => 'number',
                        'html5' => true,
                        'required' => false,
                        'attr' => [
                            'data-price' => $product->getPriceTtcOutSite(),
                            'data-command' => 'toto',
                            'min'=> 0,
                            'max'=> 10,
                            'value'=>0,
                            'pattern' => "\d*",
                            'typeProcuct' => $product->getProductType()->getType(),
                            'name' => $product->getName(),
                            'description' => $product->getDescription(),
                            'price' =>$product->getPriceTtcOutSite(). '€',
                            'class'=> 'js_form_customer_command'
                        ],
                    ]);
            }


        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('products');
    }
}