<?php

namespace App\Form;


use App\Entity\Product\Product;
use App\Entity\User\City;
use App\Entity\User\County;
use App\Entity\User\Region;
use App\Entity\User\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;


class TestType extends AbstractType
{

    /**
     * @var Security
     */
    private $security;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;


    /**
     * UserType constructor.
     *
     * @
     * @param Security $security
     * @param AuthorizationCheckerInterface $authChecker
     */
    public function __construct( Security $security,AuthorizationCheckerInterface $authChecker)
    {
        $this->security = $security;
        $this->authChecker = $authChecker;

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if($this->security->getUser() == null){
            $builder
                ->add('email', EmailType::class, [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a email',
                        ]),
                        new Email([
                            'message' => 'Invalid email address',
                        ]),
                        new Length([
                            'max' => 180,
                            'maxMessage' => 'Your email must contain at most {{ limit }} characters',
                        ])
                    ]
                ])
                ->add('userName', TextType::class, [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a userName',
                        ]),
                        new Length([
                            'max' => 180,
                            'maxMessage' => 'Your userName must contain at most {{ limit }} characters',
                        ])
                    ]
                ])
                ->add('password', PasswordType::class, [
                    'label' => 'Password',
                    'mapped' => false,
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            'max' => 255,
                            'maxMessage' => 'Your password must contain at most {{ limit }} characters',
                        ]),
                    ],
                ])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'The password fields must match.',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => true,
                    'first_options'  => ['label' => 'Password'],
                    'second_options' => ['label' => 'Repeat Password'],
                ]);
        }elseif($this->authChecker->isGranted('ROLE_SUPER_ADMIN') | $this->authChecker->isGranted('ROLE_ADMIN') | $this->authChecker->isGranted('ROLE_CUSTOMER')){
            $builder
                ->add('email', EmailType::class, [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a email',
                        ]),
                        new Length([
                            'max' => 180,
                            'maxMessage' => 'Your email must contain at most {{ limit }} characters',
                        ])
                    ]
                ])
                ->add('phone', NumberType::class,[
                    'constraints' => new NotBlank([
                        'message' => 'Please enter a phone',
                    ])
                ])
                ->add('address', TextType::class,[
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            'max' => 255,
                            'maxMessage' => 'Your password must contain at most {{ limit }} characters',
                        ]),
                    ],
                ])
                ->add('firstName', TextType::class, [
                    'label' => 'FirstName',
                    'constraints' => [
                        new Length(['min' => 3]),
                        new Regex([
                            'pattern' => '/\d/',
                            'match' => false,
                            'message' => 'Your firstname cannot contain a number',
                        ])
                    ]
                ])
                ->add('lastName', TextType::class, [
                    'label' => 'LastNane',
                    'constraints' => [
                        new Length(['min' => 3]),
                        new Regex([
                            'pattern' => '/\d/',
                            'match' => false,
                            'message' => 'Your lastName cannot contain a number',
                        ])
                    ]
                ])
                ->add('gender', ChoiceType::class, [
                    'choices' => [
                        'Homme' => 'H',
                        'Femme' => 'F'
                    ],
                    'label' => 'gender'
                ])
                ->add('region', EntityType::class,[
                    'label' => 'Region',
                    'class' => Region::class,
                    'placeholder' => 'sélectionnez votre région',
                    'mapped' => false,
                    'required' => false,
                ]);
            if($this->authChecker->isGranted('ROLE_SUPER_ADMIN')){
                $builder
                    ->add('roles', ChoiceType::class, [
                        'choices' => [
                            'Client' => 'ROLE_CUSTOMER',
                            'Administrateur' => 'ROLE_ADMIN',
                            'Super Administrateur' => 'ROLE_SUPER_ADMIN',

                        ],
                        'expanded' => true,
                        'multiple' => true,
                        'required' => false,
                        'label' => 'Rôles'
                    ])
                    ->add('isActive', CheckboxType::class);

            }
            /**
             * Retrieve the data 'region and department' data relative to the user's city
             */
            $builder->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event){
                    $data = $event->getData();
                    /**
                     * @var City $city
                     */
                    $city = $data->getCity();
                    $form = $event->getForm();
                    if ($city){
                        $county = $city->getCounty();
                        $region = $county->getRegion();
                        $this->addcounty($form, $region);
                        $this->addcity($form, $county);
                        $form->get('region')->setData($region);
                        $form->get('county')->setData($county);
                    }else{
                        $this->addcounty($form, null);
                        $this->addcity($form, null);
                    }
                }
            );
            $builder->get('region')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event){
                    $form = $event->getForm();
                    /**
                     * Add the departments in relation to the selected region
                     *
                     * @param FormInterface $form
                     * @param Region $region
                     */
                    $this->addcounty($form->getParent(), $form->getData());
                }
            );
        }

    }

    /**
     *Add a given region of conuty
     *
     * @param FormInterface $form
     * @param Region $region
     */
    private function addcounty(FormInterface $form, ?Region $region){
        dump($region);
        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'county',
            EntityType::class,
            null,
            [
                'class' => County::class,
                'label' => 'County',
                'placeholder' => $region ? 'sélectionnez votre département': 'sélectionnez d\'abord votre region',
                'mapped' => false,
                'required' => false,
                'auto_initialize' => false,
                'choices' => $region ? $region->getCounties() : [],
            ]
        );
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event){
                $form =$event->getForm();
                $this->addcity($form->getParent(), $form->getData());
            }
        );
        $form->add($builder->getForm());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }


    /**
     *  Add a given county of city
     *
     * @param FormInterface $form
     * @param County $county
     */
    private function addcity(FormInterface $form, ?County $county){
        $form->add('city', EntityType::class,[
            'label' => 'City',
            'class' => City::class,
            'placeholder' => $county ? 'sélectionnez votre villes' : 'sélectionnez d\'abord votre département',
            'choices' => $county ? $county->getCities(): []
        ]);
    }
}