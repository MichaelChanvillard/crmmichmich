<?php

namespace App\Form\Product;

use App\Entity\Product\TypeProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TypeProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('taxeEatOnSite', NumberType::class,[
                'constraints' => new NotBlank([
                    'message' => 'Please enter a tax eat on site'
                ])
            ])
            ->add('taxeEatTakeOut', NumberType::class,[
                'constraints' => new NotBlank([
                    'message' => 'Please enter a taxe eat take out'
                ])
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeProduct::class,
        ]);
    }
}
