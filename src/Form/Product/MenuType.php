<?php

namespace App\Form\Product;

use App\Entity\Product\Menu;
use App\Entity\Product\Product;
use App\Entity\Product\TypeProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a name menu',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'The name menu should be at least {{ limit }} characters',
                        'max' => 255,
                        'maxMessage' => 'The name menu must contain at most {{ limit }} characters',
                    ])
                ]
            ])
            ->add('description', TextType::class,[
                'constraints' => new Length([
                    'min' => 5,
                    'minMessage' => 'The description should be at least {{ limit }} characters ',
                    'max' => 255,
                    'maxMessage' => 'The description must contain at most {{ limit }} characters',
                ])
            ])
            ->add('discountMenu', NumberType::class,[
                'constraints' => new  NotBlank([
                    'message' => 'Please enter a number for discount menu'
                ])
            ])
            ->add('isActive', CheckboxType::class,[


            ])
            ->add('products', EntityType::class,[
                'class' => Product::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function(EntityRepository $entityRepository){
                    return $entityRepository->createQueryBuilder('p')
                        ->Where('p.isActive = 1')
                        ->leftJoin('p.productType', 'tp')
                        ->andWhere('tp.type = :valus')
                        ->setParameter('valus', 1);
                }
            ]);
            if($builder->getData()->getId() == null){
                $builder
                    ->add('dessert', EntityType::class,[
                        'class' => Product::class,
                        'choice_label' => 'name',
                        'multiple' => true,
                        'expanded' => true,
                        'mapped' => false,
                        'query_builder' => function(EntityRepository $entityRepository){
                            return $entityRepository->createQueryBuilder('p')
                                ->Where('p.isActive = 1')
                                ->leftJoin('p.productType', 'tp')
                                ->andWhere('tp.type = :valus')
                                ->setParameter('valus', 2);
                        }
                    ]);
            }


        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $events){
                $data = $events->getData();
                $form = $events->getForm();

                // liste des produits
                $products = $data->getProducts()->getValues();

                foreach ($products as $product){
                    //on verifie si il n y a pas un dessert
                    if($product->getProductType()->getType() == 2 ){
                        $this->addEditDessert($form, $products );
                        break;
                    }else{
                        $this->addDessert($form);
                    }
                }

            }
        );
    }

    /**
     * @param FormInterface $form
     */
    private function addDessert(FormInterface $form){
        $form->add('dessert', EntityType::class,[
            'class' => Product::class,
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => true,
            'mapped' => false,
            'query_builder' => function(EntityRepository $entityRepository){
                return $entityRepository->createQueryBuilder('p')
                    ->Where('p.isActive = 1')
                    ->leftJoin('p.productType', 'tp')
                    ->andWhere('tp.type = :valus')
                    ->setParameter('valus', 2);
            }
        ]);

    }

    /**
     * @param FormInterface $form
     * @param $products
     */
    private function  addEditDessert(FormInterface $form, $products){
        $this->addDessert($form);
        $form->get('dessert')->setData($products);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
