<?php

namespace App\Form\Product;

use App\Entity\Product\Product;
use App\Entity\Product\Tag;
use App\Entity\Product\TypeProduct;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a name product',
                    ]),
                    new Length([
                        'min' => (5),
                        'minMessage' => 'The name produit should be at least {{ limit }} characters',
                        'max' => 50,
                        'maxMessage' => 'The name produit tag must contain at most {{ limit }} characters',
                    ])
                    ]
            ])
            ->add('description', TextType::class,[
                'constraints' => new Length([
                    'min' => 5,
                    'minMessage' => 'The description should be at least {{ limit }} characters',
                    'max' => 255,
                    'maxMessage' => 'The description must contain at most {{ limit }} characters',
                ])
            ])
            ->add('priceHt', NumberType::class,[
                'constraints' => new NotBlank([
                    'message' => 'Please enter a price ht'
                ])
            ])
            ->add('isActive', CheckboxType::class)
            ->add('productType', EntityType::class,[
                'class' => TypeProduct::class,
                'choice_label' => 'type',
                'expanded' => false
            ])
            ->add('tags', EntityType::class,[
                'class' => Tag::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

}
