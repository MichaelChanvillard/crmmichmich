<?php

namespace App\Form\Product;

use App\Entity\Product\Tag;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class TagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'constraints' =>
                    new Length([
                    'min' => 3,
                    'minMessage' => 'The name tag should be at least {{ limit }} characters',
                    'max' => 20,
                    'maxMessage' => 'The name tag must contain at most {{ limit }} characters',
                ])
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tag::class,
        ]);
    }
}
