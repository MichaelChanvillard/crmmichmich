<?php

namespace App\Controller;

use App\Repository\Product\ProductRepository;
use App\Repository\Product\TypeProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     * @param ProductRepository $productRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {

        return $this->render('home_page/index.html.twig',[
            'current_nav' => 'Home',
            'title_page' => 'Accueil'
        ]);
    }

    /**
     * @Route("/list/product", name="list_product")
     * @param ProductRepository $productRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listProdut(TypeProductRepository $typeProductRepository, ProductRepository $productRepository)
    {
        $type = 1;

        $toto = $productRepository->getProductByProductType($type);
        $plats = $typeProductRepository->findOneBy( ['type' => 1]);
        $dessert = $typeProductRepository->findOneBy( ['type' => 2]);
        $soda = $typeProductRepository->findOneBy( ['type' => 3]);
        $alcools = $typeProductRepository->findOneBy( ['type' => 4]);
        $cafés = $typeProductRepository->findOneBy( ['type' => 5]);
        $waters = $typeProductRepository->findOneBy( ['type' => 5]);

        return $this->render('home_page/list_product.html.twig', [
            'controller_name' => 'HomePageController',
            'plats'    => $plats->getProducts(),
            'desserts' => $dessert->getProducts(),
            'sodas'    => $soda->getProducts(),
            'alcools'  => $alcools->getProducts(),
            'cafes'    => $cafés->getProducts(),
            'waters'   => $waters->getProducts(),
            'current_nav' => 'the product',
            'title_page' => 'Notre carte'

        ]);
    }

    /**
     * @Route("/legal-notice", name="legal_notice")
     */
    public function legalNotice(): Response
    {

        return$this->render('other_page/legal_notice.html.twig',[
            'title_page' => 'Mention légale'
        ]);
    }

    /**
     * @Route("/terms-of-sales", name="terms_of_sales")
     */
    public function termsOfSales(): Response
    {

        return$this->render('other_page/terms_of_sales.html.twig',[
            'title_page' => 'Conditions de vente'
        ]);
    }
}
