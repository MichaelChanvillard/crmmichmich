<?php

namespace App\Controller\Admin;

use App\Entity\Command\Command;
use App\Repository\Command\CommandRepository;
use App\Service\CommandService;
use Omnipay\Omnipay;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdminController extends AbstractController
{
    /**
     *
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     * @param CommandRepository $commandRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(CommandRepository $commandRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $commandsNotDelivred = $commandRepository->getCommandNotDelivred();
        $commandsNotPaid = $commandRepository->getCommandIsDeliveredNotPaid();

        return $this->render('admin/index.html.twig', [
            'commandsNotDelivred' => $paginator->paginate(
                $commandsNotDelivred,
                $request->query->getInt('page', 1),
                5
            ),
            'commandsNotPaid' => $paginator->paginate(
                $commandsNotPaid,
                $request->query->getInt('page', 1),
                5
            ),
            'current_slide' => 'dash bord',
            'current_slide_menu' => ''
        ]);
    }

    /**
     *
     * @Route("/admin/delivered/{id}", name="admin_delivered")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param Command $command
     * @return Response
     */
    public function delivered(Request $request, Command $command): Response
    {

        $command->changeDelivered($command);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'The command is delivered '. $command->getFinancialYear() .'-'. $command->getNumber());
        return $this->redirectToRoute('admin');
    }

    /**
     *
     * @Route("/admin/paid/{id}", name="admin_paid")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param Command $command
     * @param CommandService $commandService
     * @return void
     */
    public function paid(Request $request, Command $command, CommandService $commandService): Response
    {

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('sb-rulsp2853129_api1.business.example.com');
        $gateway->setPassword('LXXZEJSPCVGWP62F');
        $gateway->setSignature('A9HBiNXckFVVgzur-lohPkflR6yYAIJLAWrAy5dfPxlLTYKvnDWXPuzC');
        $gateway->setTestMode(true);
        $response = $gateway->purchase(
            [
                'returnUrl' => $userProfilePage = $this->generateUrl('generate_invoice', [
                    'id' => $command->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
                'cancelUrl' =>  $userProfilePage = $this->generateUrl('admin',[] , UrlGeneratorInterface::ABSOLUTE_URL),
                'description'=>'test',
                'amount' => $commandService->generatetotalpricettc($command),
                'currency' => 'EUR',
            ]
        )->send();

        if ($response->isSuccessful()) {

        } elseif ($response->isRedirect()) {
            // Redirect to offsite payment gateway
            $response->redirect();

        } else {

            // Payment failed
        }

    }

    /**
     * @Route("generate/invoice/{id}", name="generate_invoice")
     * @param Command $command
     * @param CommandService $commandService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function generateInvoice(Command $command,CommandService $commandService)
    {

        $command->setIsPaid(true);
        $this->getDoctrine()->getManager()->flush();
        $commandService->generateInvoice($command);
        $this->addFlash('success', 'Commande payer');
        return $this->redirectToRoute('admin');
    }

}
