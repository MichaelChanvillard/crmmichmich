<?php

namespace App\Controller\Product;

use App\Entity\Product\Menu;
use App\Form\Product\MenuType;
use App\Repository\Product\MenuRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product/menu")
 * @IsGranted("ROLE_ADMIN")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/", name="product_menu_index", methods={"GET"})
     * @param MenuRepository $menuRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(MenuRepository $menuRepository, PaginatorInterface $paginator, Request $request): Response
    {
        return $this->render('product/menu/index.html.twig', [
            'menus' => $paginator->paginate(
                $menus = $menuRepository->findAll(),
                $request->query->getInt('page', 1),
                5
            )
        ]);
    }

    /**
     * @Route("/new", name="product_menu_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $menu = $form->getData();

            foreach ($form->all()['dessert']->getData() as $item){
                $menu->addProduct($item);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menu);
            $entityManager->flush();

            return $this->redirectToRoute('product_menu_index');
        }

        return $this->render('product/menu/new.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_menu_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Menu $menu
     * @return Response
     */
    public function edit(Request $request, Menu $menu): Response
    {

        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $menu = $form->getData();
            foreach ($form->all()['dessert']->getData() as $item){
                $menu->addProduct($item);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_menu_index');
        }

        return $this->render('product/menu/edit.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_menu_delete", methods={"DELETE"})
     * @param Request $request
     * @param Menu $menu
     * @return Response
     */
    public function delete(Request $request, Menu $menu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$menu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($menu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_menu_index');
    }
}
