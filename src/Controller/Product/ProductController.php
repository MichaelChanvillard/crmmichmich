<?php

namespace App\Controller\Product;

use App\Entity\Product\LikeProduct;
use App\Entity\Product\Product;
use App\Form\Product\ProductType;
use App\Repository\Product\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product/product")
 * @IsGranted("ROLE_ADMIN")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_product_index", methods={"GET"})
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(ProductRepository $productRepository, PaginatorInterface $paginator, Request $request): Response
    {

        return $this->render('product/product/index.html.twig', [
            'products' => $paginator->paginate(
                $products = $productRepository->findAll(),
                $request->query->getInt('page', 1),
                5

            ),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Prouducts'
        ]);
    }

    /**
     * @Route("/new", name="product_product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        //dd($request->request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_product_index');
        }

        return $this->render('product/product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Prouducts'
        ]);
    }

    /**
     * @Route("/{id}", name="product_product_show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->render('product/product/show.html.twig', [
            'product' => $product,
            'current_slide' => 'Product',
            'current_slide_menu' => 'Prouducts'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_product_index');
        }

        return $this->render('product/product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Prouducts'
        ]);
    }

    /**
     * @Route("/{id}", name="product_product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_product_index');
    }

}
