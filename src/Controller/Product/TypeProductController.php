<?php

namespace App\Controller\Product;

use App\Entity\Product\TypeProduct;
use App\Form\Product\TypeProductType;
use App\Repository\Product\TypeProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product/type/product")
 * @IsGranted("ROLE_ADMIN")
 */
class TypeProductController extends AbstractController
{
    /**
     * @Route("/", name="product_type_product_index", methods={"GET"})
     * @param TypeProductRepository $typeProductRepository
     * @return Response
     */
    public function index(TypeProductRepository $typeProductRepository): Response
    {
        return $this->render('product/type_product/index.html.twig', [
            'type_products' => $typeProductRepository->findAll(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Taxe'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_type_product_edit", methods={"GET","POST"})
     * @param Request $request
     * @param TypeProduct $typeProduct
     * @return Response
     */
    public function edit(Request $request, TypeProduct $typeProduct): Response
    {

        $form = $this->createForm(TypeProductType::class, $typeProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_type_product_index');
        }

        return $this->render('product/type_product/edit.html.twig', [
            'type_product' => $typeProduct,
            'form' => $form->createView(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Taxe'
        ]);
    }

    /**
     * @Route("/{id}", name="product_type_product_delete", methods={"DELETE"})
     * @param Request $request
     * @param TypeProduct $typeProduct
     * @return Response
     */
    public function delete(Request $request, TypeProduct $typeProduct): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeProduct->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeProduct);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_type_product_index');
    }
}
