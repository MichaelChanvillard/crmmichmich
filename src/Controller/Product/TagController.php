<?php

namespace App\Controller\Product;

use App\Entity\Product\Tag;
use App\Form\Product\TagType;
use App\Repository\Product\TagRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product/tag")
 * @IsGranted("ROLE_ADMIN")
 */
class TagController extends AbstractController
{
    /**
     * @Route("/", name="product_tag_index", methods={"GET"})
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function index(TagRepository $tagRepository, PaginatorInterface $paginator, Request $request): Response
    {

        return $this->render('product/tag/index.html.twig', [
            'tags' => $paginator->paginate(
                $tags = $tagRepository->findAll(),
                $request->query->getInt('page', 1),
                5
            ),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Tags'
        ]);
    }

    /**
     * @Route("/new", name="product_tag_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tag);
            $entityManager->flush();

            return $this->redirectToRoute('product_tag_index');
        }

        return $this->render('product/tag/new.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Tags'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_tag_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Tag $tag
     * @return Response
     */
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_tag_index');
        }

        return $this->render('product/tag/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
            'current_slide' => 'Product',
            'current_slide_menu' => 'Tags'
        ]);
    }

    /**
     * @Route("/{id}", name="product_tag_delete", methods={"DELETE"})
     * @param Request $request
     * @param Tag $tag
     * @return Response
     */
    public function delete(Request $request, Tag $tag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tag->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tag);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_tag_index');
    }
}
