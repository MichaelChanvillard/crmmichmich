<?php

namespace App\Controller\User;

use App\Entity\Product\LikeProduct;
use App\Entity\Product\Product;
use App\Entity\User\User;
use App\Form\User\PasswordUserType;
use App\Form\User\ResetPassType;
use App\Form\User\UserType;
use App\Repository\Product\LikeProductRepository;
use App\Repository\User\UserRepository;
use App\Service\UserService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;



/**
 * @Route("/user/user", name="user_user_")
 */
class UserController extends AbstractController
{

    /**
     *
     * @Route("/", name="index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(UserRepository $userRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $users = $userRepository->findAll();

        return $this->render('user/user/index.html.twig', [
            'users' => $paginator->paginate(
                $users,
                $request->query->getInt('page', 1),
                5
            ),
            'current_slide' => 'Customer',
            'current_slide_menu' => ''
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     * @param Request $request
     * @param Security $security
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function new(Request $request,Security $security, UserPasswordEncoderInterface $passwordEncoder): Response
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('password')->getData()
                    )
                );
           if ($security->getUser() == null){
               $user->setRoles(['ROLE_CUSTOMER']);
           }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            if ($this->isGranted('ROLE_ADMIN')){
                return $this->redirectToRoute('user_user_index');
            }else{
                return $this->redirectToRoute('app_login');
            }

     }
        return $this->render('user/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_slide' => 'Customer',
            'current_slide_menu' => '',
            'title_page' => 'Création de compte '
        ]);
    }



    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_user_index');
        }

        return $this->render('user/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_slide' => 'Customer',
            'current_slide_menu' => ''
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_user_index');
    }

    /**
     * @Route("/reset/password", name="app_forgotten_password")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param MailerInterface $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function forgottenPassWord(Request $request, UserRepository $userRepository, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator): Response
    {
        $form = $this->createForm(ResetPassType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $data = $form->getData();

            $user = $userRepository->findOneBy(['email' => $data['email']]);

            if(!$user){
                $this->addFlash('danger', 'email not valid');
                return $this->redirectToRoute('customer');
            }
            $token = $tokenGenerator->generateToken();

            try{
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            }catch (\Exception $e){
                $this->addFlash('warning', 'error' . $e->getMessage());
                return $this->redirectToRoute('customer');
            }

            $url = $this->generateUrl('user_user_app_reset_password',['token' => $token],UrlGeneratorInterface::ABSOLUTE_URL);

            $email = (new Email())
                ->From('me@example.com')
                ->to($user->getEmail())
                ->subject('change password')
                ->html(
                    $this->renderView(
                        'emails/change_password.html.twig',[
                            'url' => $url,
                            'user' => $user,
                        ]
                    )
                );
            $mailer->send($email);

            $this->addFlash('success', 'L\'Email de changement de mot de passe est envoyer  à '. $user->getEmail());
            //return $this->redirectToRoute('customer');
        }

        return $this->render('security/forgotten_password.html.twig',[
            'emailForm' => $form->createView(),
            'current_slide' => 'Customer',
            'current_slide_menu' => '',
            'title_page' => 'demande de mot de pass oublier'
        ]);
    }

    /**
     * @Route("/change-password/{token}", name="app_reset_password", methods={"GET","POST"})
     * @param Request $request
     * @param $token
     * @param UserRepository $userRepository
     * @param UserService $userService
     * @return Response
     */
    public function changePassword(Request $request, $token,UserRepository $userRepository,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $userRepository->findOneBy(['resetToken' => $token]);
       // $userService->changePassword($request, $user, $route = 'user_user_index');
        $form = $this->createForm(PasswordUserType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre mot passe a été changé !');
            return $this->redirectToRoute('home_page');
        }

        return $this->render('user/user/password.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'title_page' => 'Votre nouveau mot de pass'
        ]);


    }

    /**
     * @Route("/{id}/change-password2", name="password", methods={"GET","POST"})
     * @IsGranted("ROLE_CUSTOMER")
     * @param Request $request
     * @param User $user
     * @param UserService $userService
     * @return Response
     */
    public function changePasswordConnected(Request $request, User $user, UserService $userService): Response
    {
        $userService->changePassword($request, $user, $route = 'user_user_index');
    }

    /**
     * @Route("/product/{id}/like", name="like_product")
     * @param Product $product
     * @param LikeProductRepository $likeProductRepository

     * @return Response
     */
    public function like(Product $product, LikeProductRepository $likeProductRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if(!$user) return $this->json([
            "code" => 403,
            'message' => 'Vous devez etre connecter pour faire un like '
        ],403);
        if($product->isLikedByUser($user)){

            $like =  $likeProductRepository->findOneBy([
                'product' => $product,
                'user'    => $user,
            ]);

            $entityManager->remove($like);
            $entityManager->flush();

            return $this->json([
                'code' => 200,
                'message' => 'like est bien supprimé',
                'likes' =>  $likeProductRepository->count(['product' => $product])
            ],200);
        }

        $like = new LikeProduct();
        $like->setUser($user)
            ->setProduct($product);
        $entityManager->persist($like);
        $entityManager->flush();

        return  $this->json([
            'code'  => 200,
            'message' => 'Le likes a été ajouter',
            'likes' => $likeProductRepository->count(['product' => $product])
        ],200);
    }




}
