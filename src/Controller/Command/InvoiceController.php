<?php

namespace App\Controller\Command;

use App\Entity\Command\Invoice;
use App\Form\Command\InvoiceType;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\Command\InvoiceRepository;
use App\Service\CommandService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/command/invoice")
 */
class InvoiceController extends AbstractController
{
    /**
     * @Route("/", name="command_invoice_index", methods={"GET"})
     */
    public function index(InvoiceRepository $invoiceRepository, Request $request, PaginatorInterface $paginator): Response
    {
        return $this->render('command/invoice/index.html.twig', [
            'invoices' =>  $paginator->paginate(
                $products = $invoiceRepository->findAll(),
                $request->query->getInt('page', 1),
                10

            ),
            'current_slide' => 'Invoice',
            'current_slide_menu' => ''
        ]);
    }


    /**
     * @Route("/{id}", name="command_invoice_show", methods={"GET"})
     */
    public function show(Invoice $invoice): Response
    {
        return $this->render('command/invoice/show.html.twig', [
            'invoice' => $invoice,
            'current_slide' => 'Invoice',
            'current_slide_menu' => ''
        ]);
    }

    /**
    * @Route("/pdf/{id}", name="command_invoice_pdf")
    * @param Invoice $invoice
    * @return BinaryFileResponse
    */
    public function getPDF( Invoice $invoice, CommandService $commandService): BinaryFileResponse
    {
        $commandService->generateInvoicePfd($invoice);

        return (new BinaryFileResponse('media/invoice/' . $invoice->getFinancialYear() . '/' . $invoice->getFinancialYear() . '-' . $invoice->getNumber() . '.pdf'))
            ->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_INLINE,
                $invoice->getFinancialYear() . '-' . $invoice->getNumber() . '.pdf'
            );

    }
}
