<?php

namespace App\Controller\Command;

use App\Entity\Command\Command;
use App\Form\Command\CommandType;
use App\Repository\Command\CommandRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/command/command")
 */
class CommandController extends AbstractController
{
    /**
     * @Route("/", name="command_command_index", methods={"GET"})
     * @param CommandRepository $commandRepository
     * @param Request $request

     * @return Response
     */
    public function index(CommandRepository $commandRepository,Request $request, PaginatorInterface $paginator): Response
    {
        return $this->render('command/command/index.html.twig', [
            'commands' => $paginator->paginate(
                $products = $commandRepository->getCommandNotDelivred(),
                $request->query->getInt('page', 1),
                10

            ),

            'current_slide' => 'Command',
            'current_slide_menu' => ''
        ]);
    }

    /**
     * @Route("/new", name="command_command_new", methods={"GET","POST"})
     * @param Request $request
     * @param CommandRepository $commandRepository
     * @return Response
     */
    public function new(Request $request, CommandRepository $commandRepository): Response
    {
        $user = null;
        $command = Command::createCommand($user, $commandRepository);
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(CommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $productOrderItems = $command->getOrderItems();
            foreach ($productOrderItems as $productOrderItem) {

                $command->setIsActive(true);
                if ($command->getIsEatTakeAway() == true) {
                    $taxe = $productOrderItem->getProduct()->getProductType()->getTaxeEatOnSite();
                } else {
                    $taxe = $productOrderItem->getProduct()->getProductType()->getTaxeEatTakeOut();
                }
                $productOrderItem->setTaxe($taxe);
                $productOrderItem->setPriceHt($productOrderItem->getProduct()->getPriceHt());
                $productOrderItem->setNameProduct($productOrderItem->getProduct()->getName());

            }

            $entityManager->persist($command);

            $entityManager->flush();

            return $this->redirectToRoute('command_command_index');
        }

        return $this->render('command/command/new.html.twig', [
            'command' => $command,
            'form' => $form->createView(),
            'current_slide' => 'Command',
            'current_slide_menu' => ''

        ]);
    }

    /**
     * @Route("/{id}", name="command_command_show", methods={"GET"})
     */
    public function show(Command $command): Response
    {
        return $this->render('command/command/show.html.twig', [
            'command' => $command,
            'current_slide' => 'Command',
            'current_slide_menu' => ''
        ]);
    }

    /**
     * @Route("/{id}/edit", name="command_command_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Command $command
     * @return Response
     */
    public function edit(Request $request, Command $command): Response
    {
        $form = $this->createForm(CommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($form);
            /** @var Command $command */
            $command = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $productOrderItems = $command->getOrderItems();
            foreach ($productOrderItems as $productOrderItem) {

                if(false  == $command->getOrderItems()->contains($productOrderItem)){
                    $command->removeOrderItem($productOrderItem);
                }
                if ($command->getIsEatTakeAway() == true) {
                    $taxe = $productOrderItem->getProduct()->getProductType()->getTaxeEatOnSite();
                } else {
                    $taxe = $productOrderItem->getProduct()->getProductType()->getTaxeEatTakeOut();
                }

                $productOrderItem->setTaxe($taxe);
                $productOrderItem->setPriceHt($productOrderItem->getProduct()->getPriceHt());
                $productOrderItem->setNameProduct($productOrderItem->getProduct()->getName());
                $entityManager->persist($command);

            }



            $entityManager->flush();

            return $this->redirectToRoute('command_command_index');
        }

        return $this->render('command/command/edit.html.twig', [
            'command' => $command,
            'form' => $form->createView(),
            'current_slide' => 'Command',
            'current_slide_menu' => ''
        ]);
    }

}
