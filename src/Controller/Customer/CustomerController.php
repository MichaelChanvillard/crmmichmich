<?php

namespace App\Controller\Customer;

use App\Entity\Command\Command;
use App\Form\Command\CommandCustomersType;
use App\Form\User\UserType;
use App\Repository\User\UserRepository;
use App\Service\CommandService;
use Omnipay\Omnipay;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class CustomerController extends AbstractController
{

    /**
     * @Route("/customer/command/create", name="custormer_command_create")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Request $request
     * @return Response
     */
    public function commandCustomer(Request $request): Response
    {

        $form = $this->createForm(CommandCustomersType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            return $this->redirectToRoute('customer_check_command',[
                'id' => $request->attributes->get('command')->getId()
            ]);
        }

        return $this->render('customer/command.html.twig', [
            'form' =>  $form->createView(),
            'current_nav' => 'the Command',
            'title_page' => 'Fait votre commande'

        ]);
    }

    /**
     * @route("/check/commande/{id}", name="customer_check_command" )
     * @IsGranted("ROLE_CUSTOMER")
     * @param Command $command
     * @return Response
     */
    public function CommandeCustomerCheck(Command $command){

        return $this->render('customer/valide_command.html.twig',[
            'command' => $command,
            'totalPriceTtc' => $command->generateTotalePriceCommandeTtc($command),
            'totalPriceHt' => $command->generateTotalePriceCommandeHt($command),
            'totaltaxe' => $command->generateTotalePriceCommandeTaxe($command),
            'title_page' => 'récapitulatif de votre commande'
        ]);
    }

    /**
     *
     * @Route("/customer/admin/paid/{id}", name="customer_admin_paid")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Request $request
     * @param Command $command
     * @param CommandService $commandService
     * @return void
     */
    public function paid(Request $request, Command $command, CommandService $commandService): Response
    {

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('sb-rulsp2853129_api1.business.example.com');
        $gateway->setPassword('LXXZEJSPCVGWP62F');
        $gateway->setSignature('A9HBiNXckFVVgzur-lohPkflR6yYAIJLAWrAy5dfPxlLTYKvnDWXPuzC');
        $gateway->setTestMode(true);
        $response = $gateway->purchase(
            [
                'returnUrl' => $userProfilePage = $this->generateUrl('customer_generate_invoice', [
                    'id' => $command->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
                'cancelUrl' =>  $userProfilePage = $this->generateUrl('admin',[] , UrlGeneratorInterface::ABSOLUTE_URL),
                'description'=>'test',
                'amount' => $commandService->generatetotalpricettc($command),
                'currency' => 'EUR',
            ]
        )->send();

        if ($response->isSuccessful()) {

        } elseif ($response->isRedirect()) {
            // Redirect to offsite payment gateway
            $response->redirect();

        } else {

            // Payment failed
        }

    }

    /**
     * @Route("customer/generate/invoice/{id}", name="customer_generate_invoice")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Command $command
     * @param CommandService $commandService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function generateInvoice(Command $command,CommandService $commandService)
    {
        $command->setIsActive(true);
        $command->setIsPaid(true);
        $this->getDoctrine()->getManager()->flush();
        $commandService->generateInvoice($command);
        $this->addFlash('success', 'Commande payer');
        return $this->redirectToRoute('customer_liste_command');
    }

    /**
     * @Route("customer/acounte", name="customer_acounte")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Security $security
     * @param UserRepository $userRepository
     */
    public function acountUser(Security $security, UserRepository $userRepository)
    {

        return $this->render('customer/show_profil_user.html.twig',[
            'user' => $userRepository->find($security->getUser()),
            'current_nav' => 'My account',
            'title_page' => 'Votre profile'
        ]);
    }

    /**
     * @Route("customer/acounte/edit", name="edit_customer_acounte")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Request $request
     * @param Security $security
     * @param UserRepository $userRepository
     * @return Response
     */
    public function editAcountUser(Request $request, Security $security, UserRepository $userRepository): Response
    {
        $user = $userRepository->find($security->getUser());
        $form = $this->createForm(UserType::class,$user );
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            $this->redirectToRoute('customer_acounte');
        }
        return $this->render('customer/edit_profil_user.html.twig',[
            'form' => $form->createView(),
            'user' => $user,
            'current_nav' => 'My account',
            'title_page' => 'Modifié votre profil'
        ]);
    }

    /**
     * @Route("customer/liste-command", name="customer_liste_command")
     * @param Security $security
     * @param UserRepository $userRepository
     * @return Response
     */
    public function listCommandcustomer(Security $security, UserRepository $userRepository)
    {
        return $this->render('customer/list_command_customer.html.twig',[
            'commands' => $userRepository->find($security->getUser())->getCommands(),
            'current_nav' => 'My account',
            'title_page' => 'Vos commandes'
        ]);
    }

}
