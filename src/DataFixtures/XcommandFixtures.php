<?php

namespace App\DataFixtures;


use App\Entity\Command\Command;
use App\Entity\Command\Invoice;
use App\Entity\Command\OrderItem;
use App\Repository\Command\CommandRepository;
use App\Repository\Command\InvoiceRepository;
use App\Repository\Command\OrderItemRepository;
use App\Repository\Product\MenuRepository;
use App\Repository\Product\ProductRepository;
use App\Repository\User\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class XcommandFixtures extends Fixture
{
    /**
     * @var CommandRepository
     */
    private $commandRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * XcommandFixtures constructor.
     * @param CommandRepository $commandRepository
     * @param userRepository $userRepository
     * @param ProductRepository $productRepository
     * @param MenuRepository $menuRepository
     * @param InvoiceRepository $invoiceRepository
     * @param OrderItemRepository $orderItemRepository
     */
    public function __construct(CommandRepository $commandRepository, UserRepository $userRepository, ProductRepository $productRepository, MenuRepository $menuRepository, InvoiceRepository $invoiceRepository, OrderItemRepository $orderItemRepository)
    {
        $this->invoiceRepository =$invoiceRepository;
        $this->commandRepository = $commandRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->menuRepository =$menuRepository;
        $this->orderItemRepository = $orderItemRepository;

    }

    /**
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->generateCommade($manager);
        $this->generateOrderItemProduct($manager);
//        $this->generateOrderItemMenu($manager);
        $this->generateInvoice($manager);

    }

    /**
     * @param ObjectManager $manger
     * @param Invoice $invoice
     * @param int $number
     */
    public function generateCanceledInvoice($manger, $invoice,$number )
    {

        $invoiceCanceled = clone $invoice;
        $number = $number + 1;

        $invoiceCanceled->setNumber($number);
        $invoiceCanceled->setIsInvoiceCanceled(true);
        $manger->persist($invoiceCanceled);
        $manger->flush();

    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateInvoice($manager)
    {
        for($c =0; $c < 100; $c++){
            $commad = $this->commandRepository->find(random_int(1,100));
            if($commad->getIsDelivred() == true){

                $commad->setIspaid(true);
                $dateTime = new \DateTime();
                $financialYear = $dateTime->format('Y-m-d');
                $number = $this->invoiceRepository->getLastNumberOfFinancialYear($financialYear);
                $number = $number + 1;

                $invoice = new Invoice();
                $invoice->setCommand($commad);
                $invoice->setFinancialYear($financialYear);
                $invoice->setNumber($number);
                $invoice->setIsPaid(array_rand([true, false]));
                if($invoice->getIsPaid() == true){
                    $invoice->setPaidAt($dateTime);
                    $invoice->setSendEmailAt($dateTime);
                }
                $manager->persist($invoice);
                $manager->flush();
                if($invoice->getIsPaid() == true and array_rand([true, false]) == true){
                    $invoice->setIsCanceledAt($dateTime);
                    $this->generateCanceledInvoice($manager,$invoice, $number);
                }
            }
        }
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateOrderItemProduct($manager)
    {
        for($o = 1; $o < 200; $o++){
            for ($p = 0; $p < random_int(1,5); $p++){
                $OrderItem = new OrderItem();
                $product = $this->productRepository->find(random_int(1, 28));
                $OrderItem->setProduct($product);
                $OrderItem->setQuantity(random_int(1,5));
                $OrderItem->setPriceHt($product->getPriceHt());
                $OrderItem->setTaxe($product->getProductType()->getTaxeEatOnSite());
                $OrderItem->setNameProduct($product->getName());
                $OrderItem->setCommand($this->commandRepository->find($o));
                $manager->persist($OrderItem);
            }

        }
        $manager->flush();


    }

//    /**
//     * @param ObjectManager $manager
//     * @throws \Exception
//     */
//    public function generateOrderItemMenu($manager)
//    {
//        for($o = 1; $o < 200; $o++){
//
//            for($m = 0; $m < random_int(1,2); $m++){
//                $OrderItem = new OrderItem();
//                $menu = $this->menuRepository->find(random_int(1, 4));
//                $priceHt = 0;
//                $taxe = 0;
//                foreach ($menu->getProducts() as $product){
//                    $priceHt = $priceHt + $product->getPriceHt();
//                    $taxe = $taxe + ($product->getPriceHt()/100) * ($product->getProductType()->getTaxeEatOnSite());
//                }
//                $OrderItem->setMenu($menu);
//                $OrderItem->setQuantity(random_int(1,3));
//                $OrderItem->setPriceHt($priceHt);
//                $OrderItem->setTaxe($taxe);
//                $OrderItem->setCommand($this->commandRepository->find($o));
//                $manager->persist($OrderItem);
//            }
//
//        }
//        $manager->flush();
//
//
//    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateCommade($manager)
    {
        for($c= 0; $c < 200; $c++){
            $command = new Command();
            $dateTime = new \DateTime();
            $financialYear = $dateTime->format('Y-m-d');

            $number = $this->commandRepository->getLastNumberOfFinancialYear($financialYear);
            $number = $number + 1;

            $radomeTrueFalse = random_int(0,1);


            if ($radomeTrueFalse == 1){
                $command->setIsEatTakeAway(true);
            }else{
                $command->setIsEatTakeAway(false);
            }
            $command->setNumber($number);
            $command->setIsActive(true);
            $command->setUser($this->userRepository->find(random_int(4, 20)))
                ->setCreatAt($dateTime)
                ->setFinancialYear($financialYear);

            if(random_int(0,1) == 1){
                $command->setIsDelivred( false);
            }else{
                $command->setIsDelivred(array_rand([true, false]));
            }

            $manager->persist($command);

            $manager->flush();
        }
    }

//    /**
//     * @param ObjectManager $manager
//     * @throws \Exception
//     */
//    public function generateCommade($manager)
//    {
//        for($c= 0; $c < 200; $c++){
//            $command = new Command();
//            $dateTime = new \DateTime();
//            $financialYear = $dateTime->format('Y-m-d');
//
//            $number = $this->commandRepository->getLastNumberOfFinancialYear($financialYear);
//            $number = $number + 1;
//
//            $radomeTrueFalse = random_int(0,1);
//            $totalPriceHt = 0;
//            $totalPriceTva = 0;
//            $totalPriceTtc = 0;
//
//            if ($radomeTrueFalse == 1){
//                $command->setIsEatTakeAway(true);
//            }else{
//                $command->setIsEatTakeAway(false);
//            }
//            for($m = 0; $m < random_int(1,4); $m++){
//                $menu = $this->menuRepository->find(random_int(1, 4));
//                $products = $menu->getProducts();
//                $menuPriceHt =0 ;
//                $menuPriceTva = 0;
//                $menuPriceTtc = 0;
//                foreach ($products as $product){
//                    $priceht = $product->getPriceHt() - ($product->getPriceHt()*($menu->getDiscountMenu()/100));
//                    if ($radomeTrueFalse == 1){
//                        $pricetva = $priceht * ($product->getProductType()->getTaxeEatOnSite()/100);
//                    }else{
//                        $pricetva = $priceht * ($product->getProductType()->getTaxeEatTakeOut()/100);
//                    }
//                    $pricettc = $priceht + $pricetva;
//
//                    $menuPriceHt = $menuPriceHt + $priceht;
//                    $menuPriceTva = $menuPriceTva + $pricetva;
//                    $menuPriceTtc = $menuPriceTtc + $pricettc;
//                }
//
//                $totalPriceHt = $totalPriceHt + $menuPriceHt;
//                $totalPriceTva = $totalPriceTva + $menuPriceTva;
//                $totalPriceTtc = $totalPriceTtc + $menuPriceTtc;
//
//                $command->addMenu($menu);
//            }
//            for ($p = 0; $p < random_int(1, 5); $p++){
//
//                $product = $this->productRepository->find(random_int(1, 28));
//                $priceht = $product->getPriceHt();
//
//                if ($radomeTrueFalse == 1){
//                    $pricetva = $priceht * ($product->getProductType()->getTaxeEatOnSite()/100);
//                }else{
//                    $pricetva = $priceht * ($product->getProductType()->getTaxeEatTakeOut()/100);
//                }
//                $pricettc = $priceht + $pricetva;
//
//                $totalPriceHt = $totalPriceHt + $priceht;
//                $totalPriceTva = $totalPriceTva + $pricetva;
//                $totalPriceTtc = $totalPriceTtc + $pricettc;
//
//                $command->addProduct($product);
//
//            }
//            $command->setNumber($number);
//            $command->setUser($this->userRepository->find(random_int(4, 20)))
//                ->setTotalPriceHt(number_format($totalPriceHt, 2))
//                ->setTotalTaxe(number_format($totalPriceTva, 2))
//                ->setTotalTtc(number_format($totalPriceTtc, 2))
//                ->setCreatAt($dateTime)
//                ->setFinancialYear($financialYear);
//            $command->setIsQuote(array_rand([true, false]));
//            if($command->getIsQuote() == true){
//                $command->setIsDelivred( false);
//            }else{
//                $command->setIsDelivred(array_rand([true, false]));
//            }
//
//            $manager->persist($command);
//
//            $manager->flush($command);
//        }
//    }
}
