<?php

namespace App\DataFixtures;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;;
use App\Entity\User\City;
use App\Entity\User\County;
use App\Entity\User\Region;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CityFixtures extends Fixture
{
    /**
     * Generate fixtures City, county, Region and relation
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // use php office for read Xlsx
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);

        // create liste to xlsx
        $fileCity = $reader->load('xlsx/cities.xlsx');
        $listCity = $fileCity->getactiveSheet()->toArray();
        unset($listCity[0]);

        $fileCounty = $reader->load('xlsx/departments.xlsx');
        $listCounty = $fileCounty->getactiveSheet()->toArray();
        unset($listCounty[0]);

        $fileRegion = $reader->load('xlsx/regions.xlsx');
        $listRegion = $fileRegion->getactiveSheet()->toArray();
        unset($listRegion[0]);

        // push in database region, county, city. Relation is ok
        foreach ($listRegion as $lineRegion) {

            $region = new Region();
            $region->setname($lineRegion[2]);
            $region->setCode($lineRegion[1]);
            $region->setCreateAt(new \DateTime());
            $manager->persist($region);

            foreach ($listCounty as $lineCounty) {
                {
                    if ($lineRegion[1] == $lineCounty[1]) {
                        $county = new County();
                        $county->setName($lineCounty[3]);
                        $county->setRegion($region);
                        $county->setCreateAt(new \DateTime());
                        $manager->persist($county);

                        foreach ($listCity as $lineCity) {

                            if ($lineCounty[2] == $lineCity[1]){
                                $city = new City();
                                $city->setName($lineCity[4]);
                                $city->setCounty($county);
                                if ($lineCity[3] != null) {
                                    $city->setPostalCode($lineCity[3]);
                                }
                                $city->setCreateAt(new \DateTime());
                                $manager->persist($city);
                            }
                        }
                    }
                }
            }
        }
        $manager->flush();
    }
}