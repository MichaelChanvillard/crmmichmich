<?php

namespace App\DataFixtures;


use App\Entity\Product\LikeMenu;
use App\Entity\Product\LikeProduct;
use App\Repository\Product\MenuRepository;
use App\Repository\Product\ProductRepository;
use App\Repository\User\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class XLikes extends Fixture
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * XLikes constructor.
     * @param UserRepository $userRepository
     * @param MenuRepository $menuRepository
     */
    public function __construct(UserRepository $userRepository, MenuRepository $menuRepository, ProductRepository $productRepository)
    {
        $this->userRepository = $userRepository;
        $this->menuRepository = $menuRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
         $this->generateLikesMenu($manager);
         $this->generateLikesProduct($manager);

    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateLikesMenu($manager)
    {
        foreach ($this->userRepository->findAll() as $user){

            $likeMenu = new LikeMenu();
            $likeMenu->setUser($user);
            $likeMenu->setMenu($this->menuRepository->find(random_int(1,4)));
            $manager->persist($likeMenu);
            $manager->flush();
        }
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateLikesProduct($manager)
    {
        foreach ($this->userRepository->findAll() as $user){

            $likeMenu = new LikeProduct();
            $likeMenu->setUser($user);
            $likeMenu->setProduct($this->productRepository->find(random_int(1,14)));
            $manager->persist($likeMenu);
            $manager->flush();
            $likeMenu = new LikeProduct();
            $likeMenu->setUser($user);
            $likeMenu->setProduct($this->productRepository->find(random_int(15,28)));
            $manager->persist($likeMenu);
            $manager->flush();

        }
    }


}