<?php

namespace App\DataFixtures;

use App\Entity\Product\LikeProduct;
use App\Entity\User\Contact;
use App\Entity\User\TypeAddress;
use App\Entity\User\TypeContact;
use App\Entity\User\User;
use App\Repository\Product\ProductRepository;
use App\Repository\User\CityRepository;
use App\Entity\User\Address;
use App\Repository\User\TypeAddressRepository;
use App\Repository\User\TypeContactRepository;
use App\Repository\User\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture
{
    /**
     * @var Factory
     */
    private $faker;
    /**
     * @var CityRepository
     */
    private $cityRepository;

    /**
     * @var TypeAddressRepository
     */
    private $typeAddressRepository;
    /**
     * @var TypeContactRepository
     */
    private $typeContactRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * UserFixtures constructor.
     *
     * @param CityRepository $cityRepository
     * @param TypeAddressRepository $typeAddressRepository
     * @param TypeContactRepository $typeContactRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param ProductRepository $productRepository
     * @param UserRepository $userRepository
     */
    public function __construct(CityRepository $cityRepository, TypeAddressRepository $typeAddressRepository, TypeContactRepository $typeContactRepository,UserPasswordEncoderInterface $encoder,ProductRepository $productRepository, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->cityRepository = $cityRepository;
        $this->typeContactRepository = $typeContactRepository;
        $this->typeAddressRepository = $typeAddressRepository;
        $this->faker = Factory::create("fr_FR");
        $this->encoder =$encoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->generateTypeAddress($manager);
        $this->generateTypeContact($manager);
        $this->generateUserTest($manager);
        $this->generateOtherUser($manager);

    }

    /**
     * Generate fixtures Type Address
     *
     * @param ObjectManager $manager
     */
    public function generateTypeAddress( $manager)
    {

        // liste valus Type Address
        $listTypeAddress = ['facturation', 'livraison', 'contact'];

        // Create valus Type Address
        foreach ($listTypeAddress as $typeAddress) {

            $newTypeAddress = new TypeAddress();
            $newTypeAddress->setName($typeAddress);
            $newTypeAddress->setCreatAt(new \DateTime());
            $manager->persist($newTypeAddress);
            $manager->flush($newTypeAddress);
        }
    }

    /**
     * Generate fixtures Type Contact
     *
     * @param ObjectManager $manager
     */
    public function  generateTypeContact( $manager)
    {

        // liste valus type Contact
        $listeTypeContact = ['fixe', 'moblie',];

        //Create valus Type Contact
        foreach ($listeTypeContact as $typeContact) {

            $newTypecontact = new TypeContact();
            $newTypecontact->setName($typeContact);
            $newTypecontact->setCreatAt(new \DateTime());
            $manager->persist($newTypecontact);
            $manager->flush($newTypecontact);
        }
    }

    /**
     * Generate fixtures User Test
     *
     * @param ObjectManager $manager
     */
    public function  generateUserTest( $manager)
    {
        // list compte test
        $listUser = [
            ['super.admin', ['ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CUSTOMER' ], 'password1', 'FirstSuperAdmin', 'LastSuperAdmin', 'superadmin@michmich.com', 'H', true],
            ['admin.test', ['ROLE_ADMIN','ROLE_CUSTOMER'], 'password1', 'FirstAdmin', 'LastAdmin', 'admin@michmich.com', 'F', true],
            ['director.test', ['ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_CUSTOMER' ], 'password1', 'FirstDirector', 'LastDirector', 'director@michmich.com', 'H', true],
            ['customer.test', ['ROLE_CUSTOMER' ], 'password1', 'FirstCustomer', 'LastCustomer', 'customer@michmich.com', 'H', true],
        ];

        // Create compte test
        foreach ($listUser as [$username, $roles, $password, $firstName, $lastName, $email, $gender, $isActive]) {

            $user = new User();
            $user->setEmail($email);
            $user->setRoles($roles);
            $user->setUserName($username);
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setGender($gender);
            $user->setPhone($this->faker->phoneNumber);
            $user->setAddress($this->faker->address);
            $user->setPassword($this->encoder->encodePassword(
                $user,
                $password
            ));
            $user->setIsActive($isActive);
            $user->addAddress($this->generateAddress($manager));
            $user->addContact($this->generateContact($manager));
            $user->setCity($this->cityRepository->find(random_int(1,1000)));
            $user->setCreatAt(new \DateTime());
            $manager->persist($user);
            $manager->flush();

        }
    }

    /**
     * Generate fixtures Other User
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateOtherUser( $manager)
    {

        // choise gender in array
        $genderValues = ['H', 'F'];
        //Create other user
        for ($i = 0; $i < 20; $i++) {

            $user = new User();
            $user->setEmail($this->faker->email);
            $user->setRoles(['ROLE_CUSTOMER']);
            $user->setUserName($this->faker->userName);
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setGender($genderValues[random_int(0, 1)]);
            $user->setPhone($this->faker->phoneNumber);
            $user->setAddress($this->faker->address);
            $user->setPassword($this->encoder->encodePassword(
                $user,
               'password1'
            ));
            $user->setIsActive(true);
            $user->addAddress($this->generateAddress($manager));
            $user->addContact($this->generateContact($manager));
            $user->setCreatAt(new \DateTime());
            $user->setCity($this->cityRepository->find(random_int(1,1000)));
            $manager->persist($user);
            $manager->flush();

        }
    }

    /**
     * Generate fixtures Address
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateAddress( $manager)
    {

        $newAddress = new Address();
        $newAddress->setNumber(random_int(1, 150));
        $newAddress->setStreet($this->faker->streetName);
        $newAddress->setCity($this->cityRepository->find(random_int(1, 20)));
        $newAddress->setTypeAddress($this->typeAddressRepository->find(random_int(1, 3)));
        $newAddress->setCreatAt(new \DateTime());
        $manager->persist($newAddress);
        $manager->flush($newAddress);

        return $newAddress;
    }

    /**
     * Generate fixtures Contact
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function generateContact($manager)
    {

        $newContact = new Contact();
        $newContact->setPhone($this->faker->phoneNumber);
        $newContact->setTypeContact($this->typeContactRepository->find(random_int(1, 2)));
        $newContact->setCreatAt(new \DateTime());
        $manager->persist($newContact);
        $manager->flush($newContact);

        return $newContact;
    }
    /**
     * Generate fixtures likes
     *
     * @param ObjectManager $manager
     */
    public function generateLikes($manager)
    {
        for($l = 0; $l < 20; $l++){
            $like = new LikeProduct();
            $like->setProduct($this->productRepository->findOneBy($l));
            $like->setUser($this->userRepository->findOneBy($l));
            $manager->persist($like);
            $manager->flush();
        }
    }
}
