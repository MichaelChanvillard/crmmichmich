<?php

namespace App\DataFixtures;

use App\Entity\Product\Menu;
use App\Entity\Product\Product;
use App\Entity\Product\Tag;
use App\Entity\Product\TypeProduct;
use App\Repository\Product\ProductRepository;
use App\Repository\Product\TagRepository;
use App\Repository\Product\TypeProductRepository;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductMenuFixtures extends Fixture
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var TypeProductRepository
     */
    private $typeProductRepository;
    /**
     * @var productRepository
     */
    private $productRepository;

    /**
     * ProducMenuFixtures constructor.
     * @param TagRepository $tagRepository
     * @param TypeProductRepository $typeProductRepository
     * @param productRepository $productRepository
     */
    public function __construct(TagRepository $tagRepository, TypeProductRepository $typeProductRepository, ProductRepository $productRepository)
    {
        $this->tagRepository = $tagRepository;
        $this->typeProductRepository = $typeProductRepository;
        $this->productRepository = $productRepository;

    }

    /**
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->generateTypeProduct($manager);
        $this->generateTag($manager);
        $this->generateProduct($manager);
        $this->generateMenu($manager);

    }

    /**
     * @param ObjectManager $manager
     */
    public function generateTypeProduct(ObjectManager $manager)
    {
        $listTypeProduct = [
            [1, 'plats' ,19.6, 13.5],
            [2, 'dessert', 19.6, 13.5],
            [3, 'soda', 5.5, 4.5],
            [4, 'bière', 19.6, 13.5],
            [5, 'café', 5.5, 4.5],
            [6, 'eau', 5.5, 4.5],
            ];

        foreach ($listTypeProduct as $typeProduct){

            $newTypeProduct = new TypeProduct();
            $newTypeProduct->setCreateAt(new \DateTime())
                ->setType($typeProduct[0])
                ->setTaxeEatOnSite($typeProduct[2])
                ->setTaxeEatTakeOut($typeProduct[3])
            ;

            $manager->persist($newTypeProduct);
            $manager->flush($newTypeProduct);
        }

    }

    /**
     * @param ObjectManager $manager
     */
    public function generateTag(ObjectManager $manager)
    {
        $tags = ['viande', 'poisson', 'plats', 'dessert', 'boisson', 'biére', 'café', 'tacos', 'donuts', 'hamburger'];

        foreach ($tags as $tag){

            $newTag = new Tag();
            $newTag->setName($tag)
                ->setCreateAt(new \DateTime());
            $manager->persist($newTag);
            $manager->flush($newTag);
        }

    }

    /**
     * @param ObjectManager $manager
     */
    public function generateProduct(ObjectManager $manager)
    {
        // use php office for read Xlsx
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);

        // create liste to xlsx
        $fileProduct = $reader->load('xlsx/product.xlsx');
        $listProduct = $fileProduct->getactiveSheet()->toArray();
        unset($listProduct[0]);

        foreach ($listProduct as $product){

            $newProduct = new Product();
            $newProduct->setName($product[1])
                ->setDescription($product[2])
                ->setPriceHt($product[3])
                ->setCreateAt(new \DateTime())
                ->setProductType($this->typeProductRepository->find($product[4]))
                ->setIsActive(true);
            for($i =0; $i < 3; $i++){

                $newProduct->addTag($this->tagRepository->find(random_int(1,10)));
            }


            $manager->persist($newProduct);


        }

        $manager->flush();
    }

    public function generateMenu(ObjectManager $manager)
    {
        $listMenu = [
            ['menu 01', 'description menu 01' ],  ['menu 02', 'description menu 02' ],  ['menu 03', 'description menu 03' ],  ['menu 04', 'description menu 04' ]
        ];

        foreach ($listMenu as $menu){
            $newMenu = new Menu();
            $newMenu->setCreateAt(new  \DateTime())
                ->setName($menu[0])
                ->setIsActive(true)
                ->setDescription($menu[1])
                ->setDiscountMenu(random_int(5, 10));
            for($i =0; $i < 3; $i++){
                $newMenu->addProduct($this->productRepository->find(random_int(1,20)));
            }
            $manager->persist($newMenu);
            $manager->flush($newMenu);
        }

    }
}
