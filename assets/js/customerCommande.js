
elements = document.querySelectorAll(".js_form_customer_command");

elements.forEach(element=>{

    element.onchange = (event) => {

        const result = getTotal();

        //recuperer l element a modifier et modifier avec result js-total-command
        document.querySelector('.js-total-command').textContent = `${result}`
    }


})

function getTotal(){
    let total = 0;
    elements.forEach(element=>{

        if(element.dataset.price !== undefined){
            total = total + (element.dataset.price * element.value)
        }

    })
    return new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(total)
}