
jQuery(document).ready(function() {
    var $collectionHolder = $('#command_orderItems');
    var $addTagButton = $('<button type="button" class="mt-2 btn btn-primary">Ajouter un produit</button>');




    $collectionHolder.append($addTagButton);

    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addTagButton.on('click', function(e) {

        addTagForm($collectionHolder, $addTagButton);
    });
    $collectionHolder.find('fieldset').each(function() {
        addTagFormDeleteLink($(this));
    });


});

function addTagForm($collectionHolder, $newLinkLi) {

    $proto = $('ul.product')
    var prototype = $proto.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<fieldset></fieldset>').append(newForm);
    $newLinkLi.before($newFormLi);
    addTagFormDeleteLink($newFormLi);


}

function addTagFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button class=" mb-2 remove-product  btn btn-danger" type="button">Delete this product</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}
