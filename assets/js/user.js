

/* choice region county city */



$(document).on('change', '#user_region, #user_county', function () {

    let $field = $(this)
    let $regionField = $('#user_region')
    let $form = $field.closest('form')
    let target = '#' + $field.attr('id').replace('county', 'city').replace('region', 'county')
    let data = {}

    data[$regionField.attr('name')] = $regionField.val()
    data[$field.attr('name')] = $field.val()
    console.log(data)
    $.post($form.attr('action'), data).then(function (data) {
        console.log('form', data)
        let $input = $(data).find(target)
        $(target).replaceWith($input)
    })
})

/* modal */
let currentClickedCancelBtn;
let isModalConfirmationBtnClicked = false;

$(document).on('click', '#delete_User_btn', function(e) {
    if (!isModalConfirmationBtnClicked) {
        e.preventDefault();
        currentClickedCancelBtn = $(this);
        $('#delete_User_Modal').modal('show');

    } else {
        location.href = currentClickedCancelBtn.attr('href');
    }
});

$('#delete_User_Confirmation_Btn').on('click', function() {
    isModalConfirmationBtnClicked = true;
    currentClickedCancelBtn.click();
});

$(document).on('click', '#password_User_btn', function(e) {
    if (!isModalConfirmationBtnClicked) {
        e.preventDefault();
        currentClickedCancelBtn = $(this);
        $('#password_User_Modal').modal('show');

    } else {
        location.href = currentClickedCancelBtn.attr('href');
    }
});

$('#password_User_Confirmation_Btn').on('click', function() {
    isModalConfirmationBtnClicked = true;
    currentClickedCancelBtn.click();
});

/* likes */
document.querySelectorAll('a.js-like').forEach(function (link){
    link.addEventListener('click', onClickBtnLike)
})

function onClickBtnLike(event) {
    event.preventDefault();

    const url  = this.href;
    const spanCount = this.querySelector('span.js-likes');
    const icone = this.querySelector('i');

    axios.get(url).then(function (response) {

        spanCount.textContent = response.data.likes;
        if(icone.classList.contains('fas')){
            icone.classList.replace('fas', 'far');
        }else{
            icone.classList.replace('far', 'fas');
        }
    }).catch(function (error) {
        if(error.response.status === 403){
            $('#Like_not_connect').modal('show');
        }else if (error.response.status === 500) {
            $('#Like_error_500').modal('show');
        }else {
            $('#Like_error').modal('show');
        }
    });
}

